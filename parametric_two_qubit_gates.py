from time import sleep

from qm import SimulationConfig
from qm.QmJob import QmJob

from characterization_config import *
from qm.qua import *
from qm.QuantumMachinesManager import QuantumMachinesManager

# ######################################
# %% parametric two qubit gate

qmm = QuantumMachinesManager()


def active_reset(I):
    with if_(I > 0):  # here condition can be compounded
        play('depopulate_excited', 'readout_res')
        play('pi_pulse1', 'qubit1')
    with else_():
        play('depopulate_ground', 'readout_res')


def cz_gate():
    align("trans-ee-fg", "trang-sw", "trans-cp")
    play("cw_compensation", "trans-ee-fg")
    wait(50, "trans-sw")
    play("base_pulse", "trans-cp")
    play("base_pulse", "trans-cp")


def iswap_gate():
    align("trans-ee-fg", "trang-sw", "trans-cp")
    play("cw_compensation", "trans-ee-fg")
    z_rot(np.pi, "trans-sw")
    play("base_pulse", "trans-sw")
    z_rot(-np.pi, "trans-sw")
    play("base_pulse", "trans-cp")
    play("base_pulse", "trans-cp")


def swap_gate():
    align("trans-ee-fg", "trang-sw", "trans-cp")
    play("cw_compensation", "trans-ee-fg")
    z_rot(np.pi, "trans-sw")
    play("base_pulse", "trans-sw")
    z_rot(-np.pi, "trans-sw")
    play("base_pulse", "trans-cp")
    z_rot(np.pi, "trans-cp")
    play("base_pulse", "trans-cp")
    z_rot(-np.pi, "trans-cp")


with program() as prog:
    I = declare(fixed)
    Q = declare(fixed)
    theta = declare(fixed)
    flip_control = declare(fixed)
    with for_(theta, 0, theta < np.pi, theta + 0.1):
        with for_each_(flip_control, [True, False]):
            with if_(flip_control):
                play('half_pi_pulse', 'qubit_control')

            play('half_pi_pulse', 'qubit_target')
            cz_gate()
            z_rot(theta, "qubit_target")
            play('half_pi_pulse', 'qubit_target')
            z_rot(-theta, "qubit_target")

            align('qubit_target', 'readout_res')
            measure('ro_pulse', 'readout_res', None,
                    demod.full('integ_w_cos', I, 'out1'),
                    demod.full('integ_w_sin', Q, 'out2'))

            active_reset(I)

            save(theta, 'wt_time')
            save(I, 'I')
            save(Q, 'Q')

qm = qmm.open_qm(config)
job = qm.execute(prog)
job_sim = qm.simulate(prog, SimulationConfig(duration=10000,
                                             include_analog_waveforms=True
                                             ))

samples = job_sim.simulated_analog_samples()  # can be plotted to show scope-like output

res = job.get_results()

wt_time_vals = res.variable_results.wt_time['values']
I_vals = res.variable_results.I['values']
Q_vals = res.variable_results.Q['values']
