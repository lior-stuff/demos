from time import sleep

from qm import SimulationConfig

from characterization_config import *
from qm.qua import *
from qm.QuantumMachinesManager import QuantumMachinesManager

from characterization_config import MockLO

mock_lo_generator = MockLO()

# ######################################
# %% T1 measurement

qmm = QuantumMachinesManager()

max_wait_clks = 100  # 0.4usec
step_size = 25  # 0.1usec
N_avg = 2000


def active_reset(I):
    with if_(I > 0.0):  # here condition can be compounded
        play('pi_pulse1', 'qubit1')


with program() as prog:
    wt_time = declare(int)
    n = declare(int)
    I = declare(fixed)
    Q = declare(fixed)
    with for_(var=n, init=0, cond=n < N_avg, update=n + 1):
        with for_(wt_time, step_size, wt_time <= max_wait_clks, wt_time + step_size):
            play('pi_pulse', 'qubit1')
            wait(wt_time, 'qubit1')
            align('qubit1', 'readout_res')
            measure('ro_pulse', 'readout_res', None,
                    demod.full('integ_w_cos', I, 'out1'),
                    demod.full('integ_w_sin', Q, 'out1'))

            active_reset(I)
            save(wt_time, 'wt_time')
            save(I, 'I')
            save(Q, 'Q')

qm = qmm.open_qm(config)

# execution on a real OPX:
job = qm.execute(prog)

# simulation on a fully-accurate mock OPX
job_sim = qm.simulate(prog, SimulationConfig(duration=10000,
                                             include_analog_waveforms=True
                                             ))

samples = job_sim.simulated_analog_samples()  # can be plotted to show scope-like output

# obtain measured values on PC
res = job.get_results()

wt_time_vals = res.variable_results.wt_time['values']
I_vals = res.variable_results.I['values']
Q_vals = res.variable_results.Q['values']

# #############################################
# %% resonator spectroscopy ("VNA mode")

with program() as prog:
    freq = declare(fixed)
    n = declare(int)
    I = declare(fixed)
    Q = declare(fixed)
    with for_(n, 0, n < N_avg, n + 1):
        with for_(freq, 80e6, freq <= 100e6, freq + 1e6):
            update_frequency('readout_res', freq)
            measure('ro_pulse', 'readout_res', None,
                    integration.full('integ_w_cos', I, 'out1'),
                    integration.full('integ_w_sin', Q, 'out1'))

            save(freq, 'wt_time')
            save(I, 'I')
            save(Q, 'Q')

# ################################################
# %% Ramsey measurement


qmm = QuantumMachinesManager()

max_wait_clks = 40  # 0.4usec
N_avg = 2
with program() as prog:
    wt_time = declare(int)
    n = declare(int)
    I = declare(fixed)
    Q = declare(fixed)
    with for_(n, 0, n < N_avg, n + 1):
        with for_(wt_time, step_size, wt_time <= max_wait_clks, wt_time + step_size):
            play('pi_pulse' * amp(0.5), 'qubit2')
            wait(wt_time, 'qubit2')
            play('pi_pulse' * amp(-0.5), 'qubit2')
            align('qubit2', 'readout_res')
            measure('ro_pulse', 'readout_res', None,
                    demod.full('integ_w_cos', I, 'out1'),
                    demod.full('integ_w_sin', Q, 'out2'))
            active_reset(I)
            save(wt_time, 'wt_time')
            save(I, 'I')
            save(Q, 'Q')

qm = qmm.open_qm(config)

job = qm.execute(prog)

#############################################################
# %% all combinations of sequential XY gates on a single qubit

qmm = QuantumMachinesManager()
N_avg = 2

ang_vals = [-1.0, -0.5, 0.5, 1.0]


def two_gates(axis_angle1, axis_angle2):
    z_rot(axis_angle1, 'qubit2')
    play('pi_pulse', 'qubit2')
    z_rot(-axis_angle1, 'qubit2')
    z_rot(axis_angle2, 'qubit2')  # compiler will optimize this
    play('pi_pulse' * amp(rot_ang2), 'qubit2')
    z_rot(-axis_angle2, 'qubit2')
    align('qubit2', 'readout_res')
    measure('ro_pulse', 'readout_res', None,
            demod.full('integ_w_cos', I, 'out1'),
            demod.full('integ_w_sin', Q, 'out2'))
    active_reset(I)
    save(rot_ang1, 'rot_ang1')
    save(rot_ang2, 'rot_ang2')
    save(I, 'I')
    save(Q, 'Q')


with program() as prog:
    rot_ang1 = declare(fixed)
    rot_ang2 = declare(fixed)
    n = declare(int)
    I = declare(fixed)
    Q = declare(fixed)
    seq = declare(int)
    with for_(n, 0, n < N_avg, n + 1):
        with for_(seq, 0, seq < 4, seq + 1):
            with for_each_(rot_ang1, ang_vals):
                with for_each_(rot_ang2, ang_vals):
                    with if_(seq == 0):  # XX
                        two_gates(0.0, 0.0)
                    with if_(seq == 1):  # XY
                        two_gates(0.0, np.pi / 2)
                    with if_(seq == 2):  # YX
                        two_gates(np.pi / 2, 0.0)
                    with if_(seq == 3):  # YY
                        two_gates(np.pi / 2, np.pi / 2)

qm = qmm.open_qm(config)

qm.execute(prog)

#####################################################
# %% sigmaz-sigmaz interaction

step_size = 10

qmm = QuantumMachinesManager()

max_wait_clks = 40  # 0.4usec
N_avg = 2
pi_pulse_time = 25

with program() as prog:
    wt_time = declare(int)
    n = declare(int)
    I = declare(fixed)
    Q = declare(fixed)
    pi_no_pi = declare(bool)
    with for_(n, 0, n < N_avg, n + 1):
        with for_each_(pi_no_pi, [True, False]):
            with for_(wt_time, step_size, wt_time <= max_wait_clks, wt_time + step_size):
                # play/don't play pi pulse on qubit 1
                with if_(pi_no_pi):
                    play('pi_pulse', 'qubit1')
                    align('qubit1', 'qubit2')
                with else_():
                    wait(pi_pulse_time, 'qubit1')

                # Ramsey sequence on qubit 2
                play('pi_pulse' * amp(0.5), 'qubit2')
                wait(wt_time, 'qubit2')
                play('pi_pulse' * amp(-0.5), 'qubit2')

                # readout on qubit 2
                align('qubit2', 'readout_res')
                measure('ro_pulse', 'readout_res', None,
                        demod.full('integ_w_cos', I, 'out1'),
                        demod.full('integ_w_sin', Q, 'out2'))

                active_reset(I)

                # save results
                save(wt_time, 'wt_time')
                save(I, 'I')
                save(Q, 'Q')
                save(pi_no_pi, 'pi_no_pi')

qm = qmm.open_qm(config)

qm.execute(prog)

################################################################
# %% plot program

pulse = 'cw_pulse'
factor = 4096 if pulse == 'cw_pulse' else 4096 * 16

f_if = 50e6

with program() as plot_prog:
    I = declare(fixed)
    Q = declare(fixed)
    f_qb = declare(int)

    mag_vec = declare(fixed, size=201)

    index = declare(int)
    assign(index, 0)
    with for_(f_qb, 0.8 * f_if, f_qb <= 1.2 * f_if, f_qb + 0.002 * f_if):
        update_frequency('qb1', f_qb)
        play('pulse1', 'qb1')
        measure('meas_pulse', 'readout_res', 'raw_samples', ('integ_w_cos', I), ('integ_w_sin', Q))
        save(I, 'I')
        save(Q, 'Q')
        save(f_qb, 'f')

        assign(mag_vec[index], (I * I + Q * Q) * factor + 0.1)
        assign(index, index + 1)

    with for_(index, 0, index < 201, index + 1):
        play('short_pulse' * amp(mag_vec[index]), 'qubit2')

# #############################################################################
# %% server processing

# FFT
with program() as prog:
    A = declare(fixed)
    I_stream = declare_stream()
    x = declare(fixed)
    n = declare(int)
    with for_(n, 0, n < N_avg, n + 1):
        with for_(x, 0.0, x < 1.0, x + 0.01):
            measure('my_pulse', 'RR', None, demod.full('integ_w_cos', I, 'out1'))
            save(I, I_stream)

    with stream_processing():
        I_stream.save_all("raw")
        averaged = I_stream.buffer(100).fft().average()

        averaged.save("I_av")

job = qm.execute(prog)
A_Result = job.result_handles.get("I_av")
A_Result.wait_for_all_values()
last = A_Result.fetch_all()

# histogram
with program() as prog:
    A_stream = declare_stream()
    x = declare(int)
    with for_(var=x, init=0, cond=x <= 10000, update=x + 1):
        measure('my_pulse', 'RR', None)
        save(x, A_stream)

    with stream_processing():
        A_stream.histogram([[1, 1000], [1001, 2000], [2001, 3000], [4001, 10000]]).save("hist")
        A_stream.histogram(((1, 1000), (1001, 2000), (2001, 3000), (4001, 10000))).save("hist1")
        A_stream.histogram(bins(1, 3000, 3)).save("hist2")

# dot product with constant vector
with program() as prog:
    A = declare(type)
    B = declare(type)
    A_stream = declare_stream()
    B_stream = declare_stream()
    x = declare(fixed)
    with for_(var=x, init=0.0, cond=x < 1.0, update=x + 0.01):
        measure('my_pulse', 'RR', None)
        assign(A, 0.3)
        assign(B, 0.2)
        save(A, A_stream)
        save(B, B_stream)

    with stream_processing():
        A_stream.save_all("orig_A")
        A_stream.buffer(4).map(FUNCTIONS.dot_product([0, 1, 2, 3])).save_all("dot")

# ################################################
# %% interfacing with other devices

with program() as prog:
    x = declare(fixed)
    with for_(var=x, init=0.0, cond=x < 1.0, update=x + 0.01):
        pause()
        play('my_pulse' * amp(IO1), 'RR')

job = qm.execute(prog)

lo_values = np.linspace(6e9, 7e9, 101)
amps = np.linspace(0.0, 1.0, 101)

for amp, lo_val in zip(amps, lo_values):
    mock_lo_generator.update_lo(lo_val)
    sleep(0.1)
    qm.set_io1_value(amp)
    job.resume()
