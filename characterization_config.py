from copy import deepcopy
from collections import UserDict
import numpy as np

controller = "con1"

opx_analog_outputs_to_scope = (3, 6, 7, 9)
opx_digital_outputs_to_scope = (9,)

single_pulse_len = 100
long_pulse_len = 200

wf_amp = 0.3
offsets = [0.0] * 10

lo1 = 5.525324e9
if1 = 0#150e6
lo2 = 5.18345e9
if2 = 50e6
lo_rr = 6.1423e9
if_rr = 54.2312e6
if_paramp = 100e6
lo_parmap = 3.5323e9

w = 4.0
pad = 0
w1_0 = w * np.ones(int(single_pulse_len // 4 + pad))
w2_0 = w * np.ones(int(long_pulse_len // 4 + pad))

# rotate the integration weights in the IQ plane
rot_phase = -0.3

w1_I = [np.cos(rot_phase) * w1_0, -np.sin(rot_phase) * w1_0]
w1_Q = [np.sin(rot_phase) * w1_0, np.cos(rot_phase) * w1_0]
w2_I = [np.cos(rot_phase) * w2_0, -np.sin(rot_phase) * w2_0]
w2_Q = [np.sin(rot_phase) * w2_0, np.cos(rot_phase) * w2_0]




class QmConfig(UserDict):
    def __init__(self, data):
        super().__init__(data)
        self._data_orig = deepcopy(data)
        
    def add_control_operation(self, element, operation_name, wf_i, wf_q):
        pulse_name = operation_name + '_in'
        self.data['waveforms'][pulse_name + '_i'] = {'type': 'arbitrary', 'samples': list(wf_i)}
        self.data['waveforms'][pulse_name + '_q'] = {'type': 'arbitrary', 'samples': list(wf_q)}
        self.data['pulses'][pulse_name] = {'operation': 'control', 'length': len(wf_i), 'waveforms':{'I': pulse_name + '_i', 'Q': pulse_name + '_q'}}
        self.data['elements'][element]['operations'][operation_name] = operation_name + '_in'

    def copy_measurement_operation(self, element, operation_name, new_name):
        pulse_name = self.data['elements'][element]['operations'][operation_name]
        self.data['pulses'][new_name + 'in'] = deepcopy(self.data['pulses'][pulse_name])
        self.data['elements'][element]['operations'][new_name] = new_name + 'in'
        
    def update_measurement_operation(self, element, operation_name, wf_i, wf_q):
        pulse_name = self.data['elements'][element]['operations'][operation_name]
        self.data['waveforms'][pulse_name + '_i'] = {'type': 'arbitrary', 'samples': list(wf_i)}
        self.data['waveforms'][pulse_name + '_q'] = {'type': 'arbitrary', 'samples': list(wf_q)}
        self.data['pulses'][pulse_name]['waveforms'] = {'I': pulse_name + '_i', 'Q': pulse_name + '_q'}
        self.data['pulses'][pulse_name]['length'] = len(wf_i)

    def reset(self):
        self.data = deepcopy(self._data_orig)

        
config = QmConfig({
    'version': 1,
    'controllers': {
        controller: {
            'type': 'opx1',
            'analog_outputs': {
                i + 1: {'offset': offsets[i]} for i in range(10)
            },
            'analog_inputs': {
                1: {'offset': 0.0},
                2: {'offset': 0.0}
            }
        }
    },

    'elements': {
        'qubit1': {
            'mixInputs': {
                'I': ('con1', 1),
                'Q': ('con1', 2),
                'lo_frequency': lo1,
                'mixer': 'mxr_qb1'
            },
            'intermediate_frequency': if1,
            'operations': {
                'pulse1': 'pulse1_in',
                'pi_pulse': 'pi_pulse_qb1_in',
            },
        },
        'qubit2': {
            'mixInputs': {
                'I': ('con1', 3),
                'Q': ('con1', 4),
                'lo_frequency': lo2,
                'mixer': 'mxr_qb2'
            },
            'intermediate_frequency': if2,
            'operations': {
                'pulse1': 'pulse1_in',
                'pi_pulse': 'pi_pulse_qb2_in',
            },
        },
        'readout_res': {
            'mixInputs': {
                'I': ('con1', 7),
                'Q': ('con1', 8),
                'lo_frequency': lo_rr,
                'mixer': 'mxr_rr'
            },
            'intermediate_frequency': if_rr,
            'operations': {
                'pulse1': 'pulse1_in',
                'ro_pulse': 'meas_pulse_in',
                'ro_pulse_long': 'long_meas_pulse_in'
            },
            'time_of_flight': 200,
            'smearing': 0,
            'outputs': {
                'out1': ('con1', 1),
                'out2': ('con1', 2)
            }

        },

    },
    'pulses': {
        'pulse1_in': {
            'operation': 'control',
            'length': single_pulse_len,
            'waveforms': {
                'I': 'wf1',
                'Q': 'wf_zero',
            },
        },
        'pi_pulse_qb1_in': {
            'operation': 'control',
            'length': single_pulse_len,
            'waveforms': {
                'I': 'wf1',
                'Q': 'wf_zero',
            },
        },
        'pi_pulse_qb2_in': {
            'operation': 'control',
            'length': single_pulse_len,
            'waveforms': {
                'I': 'wf1',
                'Q': 'wf_zero',
            },
        },
        'meas_pulse_in': {
            'operation': 'measurement',
            'length': single_pulse_len,
            'waveforms': {
                'I': 'wf_meas',
                'Q': 'wf_zero',
            },
            'integration_weights': {
                'integ_w_I': 'integW1_I',
                'integ_w_Q': 'integW1_Q',
            },
            'digital_marker': 'trig_wf0'
        },
        'long_meas_pulse_in': {
            'operation': 'measurement',
            'length': long_pulse_len,
            'waveforms': {
                'I': 'wf_meas',
                'Q': 'wf_zero',
            },
            'integration_weights': {
                'integ_w_I': 'integW2_I',
                'integ_w_Q': 'integW2_Q',
            },
            'digital_marker': 'trig_wf0'
        },
    },

    'digital_waveforms': {
        'trig_wf0': {'samples': [(1, single_pulse_len), (0, 0)]}
    },

    'waveforms': {
        'wf1': {
            'type': 'constant',
            'sample': wf_amp
        },
        'wf_meas': {
            'type': 'constant',
            'sample': wf_amp
        },
        'wf_zero': {
            'type': 'constant',
            'sample': 0.0
        },
    },

    'integration_weights': {
        'integW1_I': {
            'cosine': w1_I[0].tolist(),
            'sine': w1_I[1].tolist(),
        },
        'integW1_Q': {
            'cosine': w1_Q[0].tolist(),
            'sine': w1_Q[1].tolist(),
        },
        'integW2_I': {
            'cosine': w2_I[0].tolist(),
            'sine': w2_I[1].tolist(),
        },
        'integW2_Q': {
            'cosine': w2_Q[0].tolist(),
            'sine': w2_Q[1].tolist(),
        },
    },

    'mixers': {
        'mxr_rr': [
            {'intermediate_frequency': if_rr, 'lo_frequency': lo_rr, 'correction': [1.0, 0.0, 0.0, 1.0]},
            {'intermediate_frequency': 0.0, 'lo_frequency': lo_rr, 'correction': [1.0, 0.0, 0.0, 1.0]},
        ],
        'mxr_qb1': [
            {'intermediate_frequency': if1, 'lo_frequency': lo1,
             'correction': [1.0, 0.0, 0.0, 1.0]},
            {'intermediate_frequency': 0.0, 'lo_frequency': lo1,
             'correction': [1.0, 0.0, 0.0, 1.0]},
        ],
        'mxr_qb2': [
            {'intermediate_frequency': if2, 'lo_frequency': lo2,
             'correction': [1.0, 0.0, 0.0, 1.0]},
            {'intermediate_frequency': 0.0, 'lo_frequency': lo2,
             'correction': [1.0, 0.0, 0.0, 1.0]},
        ],
        'mxr_paramp': [
            {'intermediate_frequency': if_paramp, 'lo_frequency': lo_parmap,
             'correction': [1.0, 0.0, 0.0, 1.0]}
        ]
    },

})
